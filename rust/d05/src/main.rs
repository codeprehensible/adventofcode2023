use std::io::{BufReader, BufRead};
use std::{env, fs, i64};

fn main() {
    let args: Vec<String> = env::args().collect();
    let infile = &args[1];

    let lines = read_file_lines(infile);

    let almanac = parse_almanac(lines);
    let seed_maps = almanac.build_map().unwrap();

    // finding the smallest location
    let mut location_vec: Vec<usize> = vec![];
    for smap in seed_maps.iter() {
        location_vec.push(smap.location);
    }

    let min_location: usize = *location_vec.iter().min().unwrap();
    println!("Minimal location: {}", min_location);
}


struct SeedMap {
    seed: usize,
    soil: usize,
    fertilizer: usize,
    water: usize,
    light: usize,
    temperature: usize,
    humidity: usize,
    location: usize,
}

#[derive(Debug)]
struct AlmanacRange {
    source: usize,
    target: usize,
    step: usize,
}

impl AlmanacRange {
    fn get_target(&self, source: usize) -> Result<usize, Box<dyn std::error::Error>> {
        assert!(self.check_in_range(source));

        let diff = source - self.source;
        let target = self.target + diff;
        
        Ok(target)
    }

    fn check_in_range(&self, source: usize) -> bool {
        return (source >= self.source) & (source <= self.source + self.step)
    }
}

#[derive(Debug)]
struct Almanac {
    seed: Vec<usize>,
    soil: Vec<AlmanacRange>,
    fertilizer: Vec<AlmanacRange>,
    water: Vec<AlmanacRange>,
    light: Vec<AlmanacRange>,
    temperature: Vec<AlmanacRange>,
    humidity: Vec<AlmanacRange>,
    location: Vec<AlmanacRange>,
}

#[derive(Debug, Clone)]
struct SeedError;

fn get_target_almanac_range(source: &usize, ar: &Vec<AlmanacRange>) -> usize {
    assert!(ar.len() > 0);
    let target: usize = {
        let mut tmp: i64 = -1;
        for s in ar {
            if !s.check_in_range(*source) { 
                continue; 
            } else {
                tmp = s.get_target(*source).unwrap().try_into().unwrap();
            }
        }
        if tmp == -1 {
            tmp = *source as i64;
        }
        tmp.try_into().unwrap()
    };

    return target
}

impl Almanac {
    fn build_map(&self) -> Result<Vec<SeedMap>, SeedError> {

        // check whether there are any seeds stored 
        // in the object
        if self.seed.len() == 0 {
            return Err(SeedError);
        }

        let mut seed_maps: Vec<SeedMap> = vec![];

        for seed in self.seed.iter() {
            // get soil id 
            let soil = get_target_almanac_range(&seed, &self.soil);

            let fertilizer = get_target_almanac_range(&soil, &self.fertilizer);

            let water = get_target_almanac_range(&fertilizer, &self.water);

            let light = get_target_almanac_range(&water, &self.light);

            let temperature = get_target_almanac_range(&light, &self.temperature);

            let humidity = get_target_almanac_range(&temperature, &self.humidity);

            let location = get_target_almanac_range(&humidity, &self.location);

            let seedmap: SeedMap = SeedMap { 
                seed: *seed, 
                soil, 
                fertilizer, 
                water, 
                light, 
                temperature, 
                humidity, 
                location,
            };
            seed_maps.push(seedmap);
        }

        return Ok(seed_maps)
    }


    fn set(&mut self, field: &str, value: AlmanacRange){
        if field == "soil" {
            self.soil.push(value);
            return 
        } else if field == "fertilizer" {
            self.fertilizer.push(value);
            return
        } else if field == "water" {
            self.water.push(value);
            return
        } else if field == "light" {
            self.light.push(value);
            return 
        } else if field == "temperature" {
            self.temperature.push(value);
            return
        } else if field == "humidity" {
            self.humidity.push(value);
            return;
        } else if field == "location" {
            self.location.push(value);
            return
        } else {
            panic!("Field {} not found", field)
        }
    }
}

fn parse_almanac(lines: Vec<String>) -> Almanac {

    // first line contains seed ids:
    let seeds: Vec<usize> = lines[0].replace("seeds: ", "")
        .split(" ")
        .into_iter()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();

    let mut almanac: Almanac = Almanac {
        seed: vec![],
        soil: vec![],
        fertilizer: vec![],
        water: vec![],
        light: vec![],
        temperature: vec![],
        humidity: vec![],
        location: vec![],
    };

    let mut current_type = "";

    for line in &lines[1..] {
        let prev_type = current_type;
        current_type = match line.as_str() {
            "seed-to-soil map:" => "soil",
            "soil-to-fertilizer map:" => "fertilizer",
            "fertilizer-to-water map:" => "water",
            "water-to-light map:" => "light",
            "light-to-temperature map:" => "temperature",
            "temperature-to-humidity map:" => "humidity",
            "humidity-to-location map:" => "location",
            "" => "",
            _ => current_type,
        };

        if current_type == "" {
            continue;
        } else {
            if prev_type == current_type {
                let data: Vec<usize> = line.as_str()
                    .split(" ")
                    .into_iter()
                    .map(|s| s.parse::<usize>().unwrap())
                    .collect();
                assert_eq!(data.len(), 3);
                let tmp_range = AlmanacRange{source: data[1], target: data[0], step: data[2]};
                almanac.set(current_type, tmp_range);
            } else {
                continue;
            }
        }

    }

    almanac.seed = seeds;
       
    return almanac
}

fn read_file_lines(filepath: &str) -> Vec<String> {

    let file = fs::File::open(filepath).expect("file not found");
    let buffered = BufReader::new(file);

    let mut tmp = vec![];

    for line in buffered.lines() {
        tmp.push(line.unwrap());
    };

    return tmp
}

