use regex::Regex;
use std::fs;
use std::io::BufReader;
use std::io::BufRead;



fn main() {

    let file = fs::File::open("calib.txt").expect("file not found");
    let buffered = BufReader::new(file);

    let mut tmp = vec![];

    for line in buffered.lines() {
        tmp.push(line.unwrap())
    };

    let list_of_calib_strings = tmp.iter()
        .map(|s| s as &str)
        .collect();

    // let list_of_calib_strings = vec!["1abc2","pqr3stu8vwx","a1b2c3d4e5f","treb7uchet"];

    let res = decode_calib(list_of_calib_strings);

    println!("{}", res);
}

#[allow(dead_code)]
fn decode_calib(lns: Vec<&str>) -> usize {

    let re = Regex::new(r"([0-9]|one|two|three|four|five|six|seven|eight|nine|ten)").unwrap();

    let mut found_ints: Vec<usize> = vec![];
    for l in lns {
        let mut findings: Vec<&str> = vec![];

        let line = decode_string_nums(l);

        for (_, [i]) in re.captures_iter(line.as_str()).map(|c| c.extract()) {
            findings.push(i);
        }

        let mut ints: Vec<&str> = vec![];

        for i in findings {
            ints.push(i);
        }

        if ints.len() > 1 {
            let mut tmp = ints[0].to_owned();
            tmp.push_str(ints[ints.len()-1]);
            found_ints.push(tmp.parse::<usize>().unwrap());
        } else if ints.len() == 1 {
            let mut tmp = ints[0].to_owned();
            tmp.push_str(ints[0]);
            found_ints.push(tmp.parse::<usize>().unwrap());
        }

    }

    let dsum: usize = found_ints.iter().sum();

    return dsum
}

fn decode_string_nums(x: &str) -> String {
    let re = Regex::new(r"(one|two|three|four|five|six|seven|eight|nine|ten)").unwrap();

    let mut x_owned = x.to_owned();
    // iteravely replace string numbers with their numeric counterpart
    // by replacing the first letter of the string with the number
    // do this until all possible string numbers are removed
    
    loop {
        let mut findings = vec![];
        for (_, [i]) in re.captures_iter(&x_owned).map(|y| y.extract()) {
            findings.push(i.to_string());
        }

        if findings.len() == 0 {
            break;
        } else {
            for f in findings.iter() {
                x_owned = match f.as_str() {
                    "one" => x_owned.replace(f, "1ne"),
                    "two" => x_owned.replace(f, "2wo"),
                    "three" => x_owned.replace(f, "3hree"),
                    "four" => x_owned.replace(f, "4our"),
                    "five" => x_owned.replace(f, "5ve"),
                    "six" => x_owned.replace(f, "6ix"),
                    "seven" => x_owned.replace(f, "7even"),
                    "eight" => x_owned.replace(f, "8ight"),
                    "nine" => x_owned.replace(f, "9ine"),
                    _ => x_owned,
                }
            }
        }
    }

    return x_owned
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_decode_calib() {
        let inp = vec![
            "two1nine",
            "eightwothree",
            "abcone2threexyz",
            "xtwone3four",
            "4nineeightseven2",
            "zoneight234",
            "7pqrstsixteen",
        ];
        assert_eq!(decode_calib(inp), 281);
    }

    #[test]
    fn test_decode_string_nums() {
        assert_eq!(decode_string_nums("twone"), String::from("2w1ne"))
    }
}
