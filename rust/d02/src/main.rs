use std::{fs, io::BufReader, io::BufRead, usize};
use regex::Regex;

fn main() {
    let lines = read_file_lines("games.txt");

    let mut possible_games: Vec<usize> = vec![];
    let mut powers: Vec<usize> = vec![];
    for line in lines.iter() {
        possible_games.push(check_if_possible(line));

        let bag = decompose_game_string(line);
        let power = calc_power(bag);
        powers.push(power);
    }

    println!("Sum of possible games: {}", possible_games.iter().sum::<usize>());
    println!("Sum of powers: {}", powers.iter().sum::<usize>());
}

#[derive(Debug)]
struct Bag {
    red: usize,
    green: usize,
    blue: usize,
}

fn calc_power(bag: Bag) -> usize {
    return bag.red * bag.green * bag.blue
}

fn decompose_game_string(game: &str) -> Bag {

    let mut bag = Bag {
        red: 0,
        green: 0,
        blue: 0,
    };

    for color in vec!["red", "green", "blue"] {
        let regex_string = format!(r"(\d+ {color})");

        let re = Regex::new(&regex_string).unwrap();

        let mut matches: Vec<usize> = vec![];
        for (_, [i]) in re.captures_iter(game).map(|s| s.extract()) {
            let i_split: Vec<&str> = i.split(" ").collect();
           matches.push(i_split[0].parse::<usize>().unwrap()); 
        }

        if color == "red" {
            bag.red = *matches.iter().max().unwrap();
        } else if color == "green" {
            bag.green = *matches.iter().max().unwrap();
        } else {
            bag.blue = *matches.iter().max().unwrap();
        }
    }

    // decompose color and n
    return bag
}

fn read_file_lines(filepath: &str) -> Vec<String> {

    let file = fs::File::open(filepath).expect("file not found");
    let buffered = BufReader::new(file);

    let mut tmp = vec![];

    for line in buffered.lines() {
        tmp.push(line.unwrap());
    };

    return tmp
}

fn check_if_possible(game: &str) -> usize {
    let n_red = 12;
    let n_green = 13;
    let n_blue = 14;

    let tmp: Vec<&str> = game.split(": ").collect();
    let n_game = tmp[0].replace("Game ", "").parse::<usize>().unwrap();

    // get individual results of the game
    let results: Vec<&str> = tmp[1].split("; ").collect();

    let mut game_is_possible = true;
    for result in results {
        let n_colors: Vec<&str> = result.split(", ").collect();

        for n_color in n_colors {
            let nc: Vec<&str> = n_color.split(" ").collect();
            let n = nc[0].parse::<usize>().unwrap();
            let color = nc[1].replace(",", "");


            let possible = match color.as_str() {
                "red" => n <= n_red,
                "green" => n <= n_green,
                "blue" => n <= n_blue,
                _ => false,
            };

            if !possible {
                game_is_possible = false;
            }
        }


    }


    if game_is_possible {
        return n_game 
    } else {
        return 0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_if_possible() {
        let inp_possible = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let inp_impossible = "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"; 
        let inp_possible2 = "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";


        assert_eq!(check_if_possible(inp_possible), 1);
        assert_eq!(check_if_possible(inp_impossible), 0);
        assert_eq!(check_if_possible(inp_possible2), 5);
    }
}
