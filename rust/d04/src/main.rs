use std::{env, fs};
use std::io::{BufReader, BufRead};
use regex::Regex;

fn main() {
    let args: Vec<String> = env::args().collect();
    let infile = &args[1];
    let lines = read_file_lines(infile);

    let mut score: Vec<usize> = vec![];
    let mut cards: Vec<Card> = vec![];
    for line in lines {
        let card = Card::new(&line);
        cards.push(card.clone());
        score.push(card.calc_part1());
    }

    println!("Score, part 1: {}", score.iter().sum::<usize>());

    println!("N cards, part2: {}", part2(cards));
}

fn read_file_lines(filepath: &str) -> Vec<String> {

    let file = fs::File::open(filepath).expect("file not found");
    let buffered = BufReader::new(file);

    let mut tmp = vec![];

    for line in buffered.lines() {
        tmp.push(line.unwrap());
    };

    return tmp
}

fn part2(cards: Vec<Card>) -> usize {

    let mut n_cards: Vec<usize> = vec![1; cards.len()];

    for (i, card) in cards.iter().enumerate() {
        let n_wins = card.n_winners();
        // println!("{}", n_wins);
        
        if n_wins == 0 {
            continue;
        }


        let current_card_n = n_cards[i];
        for j in i+1..(i + n_wins+1) {
            n_cards[j] += current_card_n;
        }
        // println!("{:?}", n_cards);
    }

    // println!("{:?}", n_cards);

    return n_cards.iter().sum::<usize>();
}

#[derive(Clone)]
struct Card {
    winning: Vec<usize>,
    draws: Vec<usize>
}

trait Part2 {
    fn n_winners(&self) -> usize;
}

impl Part2 for Card {
    fn n_winners(&self) -> usize {
        let mut wins: Vec<usize> = vec![];
        for num in self.draws.iter() {
            if self.winning.contains(num) {
                wins.push(*num)
            }
        }

        return wins.len()
    }

}

trait Methods {
    fn new(draw_line: &str) -> Card;

    fn calc_part1(&self) -> usize;
}

impl Methods for Card {
    fn calc_part1(&self) -> usize {
        let mut wins: Vec<usize> = vec![];
        
        for num in self.draws.iter() {
            if self.winning.contains(num) {
                wins.push(*num)
            }
        }


        let score: usize = if wins.len() == 0 {
            0 
        } else if wins.len() == 1 {
            1
        } else {
            let base: usize = 2;
            base.pow((wins.len()-1).try_into().unwrap())
        };

        return score;
    }

    fn new(draw_line: &str) -> Card {

        let card_num: Vec<&str> = draw_line.split(": ").collect();
        let num_str_raw: Vec<&str> = card_num[1].split(" | ").collect();
        let mut num_str: Vec<String> = vec![];
        let re = Regex::new(r" +").unwrap();
        let re_start = Regex::new(r"^ +").unwrap();

        for elem in num_str_raw {
            let tmp_1 = re.replace_all(elem, " ");
            let tmp_2 = re_start.replace_all(&tmp_1, "");
            num_str.push(tmp_2.to_string());
        }


        let win: Vec<usize> = num_str[0].split(" ")
            .map(|n| n.parse::<usize>().unwrap())
            .collect();
        let draw: Vec<usize> = num_str[1].split(" ")
            .map(|n| n.parse::<usize>().unwrap())
            .collect();

        return Card { winning: win, draws: draw }
    }
}
