use std::{fs, io::BufReader, io::BufRead, usize};
use regex::Regex;
// the plan:
// move through the input
// identify numbers
// check vincinity of the numbers for symbols
// return vector of numbers
fn main() {
    let lines = read_file_lines("input.txt");
    let valid_numbers = parse_input(lines.clone());
    let gear_ratios = get_gear_ratios(lines);

    //println!("{:?}", gear_ratios);
    println!("Sum: {}", valid_numbers.iter().sum::<usize>());
    println!("Sum GR: {}", gear_ratios.iter().sum::<usize>());
}

fn get_gear_ratios(lines: Vec<String>) -> Vec<usize> {
    let mut valid_numbers: Vec<usize> = vec![];
    let re_num = Regex::new(r"(\d+)").unwrap();
    let re_sym = Regex::new(r"(\*)").unwrap();

    for (ptr_row, line) in lines.iter().enumerate() {
        for m in re_sym.captures_iter(line) {
            if re_sym.is_match(line) {
                let i = m.get(0).unwrap().start();
                let _i_end = m.get(0).unwrap().end();

                let mut surrounding_lines: Vec<&str> = vec![];
                let mut possible_num: Vec<usize> = vec![];

                if ptr_row == 0 {
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row+1]);
                } else if ptr_row + 1 < lines.len() {
                    surrounding_lines.push(&lines[ptr_row-1]);
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row+1]);
                } else {
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row-1]);
                }

                for l in surrounding_lines {
                    if re_num.is_match(l){
                        // find all numbers
                        for found_num_match in re_num.captures_iter(l) {
                            let found_num = found_num_match.get(0).unwrap();
                            let num = found_num.as_str().parse::<usize>().unwrap();
                            let num_start = if found_num.start() == 0 {0} else {found_num.start()-1};
                            let num_range = num_start..found_num.end()+1;
                            if num_range.contains(&i) {
                                possible_num.push(num);
                            }
                        }
                    }
                }

                if possible_num.len() == 2 {
                    valid_numbers.push(possible_num[0] * possible_num[1]);
                }
            }
        }
    }

    return valid_numbers
}


fn parse_input(lines: Vec<String>) -> Vec<usize> {

    let mut valid_numbers: Vec<usize> = vec![];
    let re_num = Regex::new(r"(\d+)").unwrap();
    let re_sym = Regex::new(r"([^0-9\.])").unwrap();
    for (ptr_row, line) in lines.iter().enumerate() {

        for m in re_num.captures_iter(line) {

            if re_num.is_match(line) {
                let i = m.get(0).unwrap().start();
                let i_end = m.get(0).unwrap().end();
                let s = m.get(0).unwrap().as_str();
                let s_usize = s.parse::<usize>().unwrap();

                let mut surrounding_lines: Vec<&str> = vec![];

                if ptr_row == 0 {
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row+1]);
                } else if ptr_row + 1 < lines.len() {
                    surrounding_lines.push(&lines[ptr_row-1]);
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row+1]);
                } else {
                    surrounding_lines.push(line);
                    surrounding_lines.push(&lines[ptr_row-1]);
                }

                // checking for previous line
                for surr_line in surrounding_lines {
                    let prev_line_start = if i > 0 {
                        i - 1
                    } else {
                        0
                    };
                    let prev_line_stop = if i_end+1 < surr_line.len() {
                        i_end + 1
                    } else {
                        surr_line.len()
                    };

                    if re_sym.is_match(&surr_line[prev_line_start..prev_line_stop]) {
                        valid_numbers.push(s_usize);
                    }
                }
            }
        }
    }
    
    return valid_numbers
}

fn read_file_lines(filepath: &str) -> Vec<String> {

    let file = fs::File::open(filepath).expect("file not found");
    let buffered = BufReader::new(file);

    let mut tmp = vec![];

    for line in buffered.lines() {
        tmp.push(line.unwrap());
    };

    return tmp
}
