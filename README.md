# Advent of code 2023 challenge
This is my first time doing the [advent of code](https://adventofcode.com/) challenge.
And as I am trying to do something outside the typical problem solving, I set a custom goal to myself: Answer all challenges in the following languages:

1. rust (beginner)
2. golang (intermediate)
3. python (proficient)

Since this is a lot of work, and I still need to do a normal job, I set the time for all three languages to end of **November 2024**. 
Any day earlier, is a win!

Since I want to use the opportunity to learn something new, I will answer the tasks first in rust, which is a fairly new language to me, then in golang, with which I worked a bit earlier, and then in python, which is my go-to language.

